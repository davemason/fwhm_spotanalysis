# FWHM Analysis of Spots #
This IJ1 script is written to automate quantitation of small spot-like structures. Originally written to measure the FWHM of nuclear speckles. The script does the following:

* Open a user specified image with [Script Parameter](https://imagej.net/Script_Parameters) input
* Run [Find Maxima](https://imagej.nih.gov/ij/docs/guide/146-29.html#toc-Subsection-29.4) outputing a point selection
* Record X and Y coordinates of points
* For each point draw a horizonal line through the point, a user-specified length and fit a single-component gaussian to the profile
    * If that has poor R^2, repeat with a vertical line, if that fails, skip
* Calculate the FWHM if this is greater than 10x the expected value, discard, else write out to a custom results table
* As of commit 93ac6c5, includes a simple spot navigator that can be used to view individual spots

Written and Tested in [Fiji](http://fiji.sc) 64bit.

### Usage ###

* Download and update [Fiji](http://fiji.sc)
* Open the script in the Fiji Script Editor and hit run, you'll have the following options:
    * Input file
    * Tolerance: The tolerance used by Find Maxima, calculate this ahead of time using Find Maxima with the 'preview' option.
    * Total Line Length (pixels): self explanatory. If this is too low, you will have artificially large FWHM. The only problem with going too big comes when you have dense particles.
    * GoF cutoff: R-squared value for gaussian fit. Fits below this number (after checking horizonal and vertical) will be exluded. Or most purposes ~0.85 should work fine.
    * Estimated FWHM: Used to catch spots with good fit but overestimated size (mostly caused by large or aggregated objects). Will exlude if the FWHM is measured as >10x this value. Set to zero to ignore.
	* Mark Excluded: Will draw an overlay onto the image outlining where are spots that failed the GoF test (red square) and which are excluded due to size (yellow square)
	* Show Range: Allows marking of spots within a specified size range
	* Open Spot Navigator: When analysis is complete, optionally open a simple navigator that allows you to view particular spots identified by coordinates or size
    * Verbose: Will print out an analysis report to the log window showing coordinates, categorisation (Horizontal fit, vertical fit, Excluded, Too big) as well as R-squared values. If Spot Navigator is selected, will also record any spots visited.
* Hit OK and the script will run, leaving the image window and a custom results table (called FWHM) open. The values are returned in the units of the original image.

### Acknowledgements and Licence ###

* The code was written by [Dave Mason](http://pcwww.liv.ac.uk/~dnmason), from the University of Liverpool [Centre for Cell Imaging](http://cci.liv.ac.uk)
* Provided under a [CCBY 4.0 Licence](https://creativecommons.org/licenses/by/4.0/) which allows use and modification as long as the original source is attributed.
